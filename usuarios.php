<?php 

    include 'includes/conexion.php';

    $query = "SELECT * FROM usuario";
    $consulta_usuarios = $conexion->query($query);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php'; ?>
    <title>Listado de usuarios</title>
</head>
<body style="background: #dfdfdf;">

    <div class="contenedor">
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-bordered" id="usuarios">
                <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Paterno</th>
                        <th scope="col">Materno</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Edad</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        if($consulta_usuarios->num_rows > 0){
                            while ($usuarios = $consulta_usuarios->fetch_assoc()){
                    ?>
                    <tr>
                        <td> <?php echo $usuarios['nombre'] ?></td>
                        <td> <?php echo $usuarios['paterno'] ?></td>
                        <td> <?php echo $usuarios['materno'] ?></td>
                        <td> <?php echo $usuarios['correo'] ?></td>
                        <td> <?php echo $usuarios['edad'] ?></td>
                    </tr>

                    <?php }} ?>
                </tbody>
            </table>
        </div>
    </div>



    <?php require 'extensiones/scripts.php'; ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#usuarios').DataTable({
                language: {
                    search: "Buscar:",
                    paginate: {
                        first: "Primer",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último"
                    },
                    info: "Mostrando del _START_ al _END_ de _TOTAL_ resultados disponibles",
                    emptyTable: "No existen elementos para mostrar en la tabla",
                    infoEmpty: "Mostrando del 0 al 0 de 0 resultados",
                    infoFiltered: "(Filtrado de _MAX_ resultados)",
                    lengthMenu: "Mostrando _MENU_ resultados",
                    loadingRecords: "Cargando...",
                    processing: "Procesando...",
                    zeroRecords: "No se encontraron resultados",
                    aria: {
                        sortAscending: ": Ordenado de forma ascendente",
                        sortDescending: ": Ordenado de forma descendente"
                    }

                }
            });
        });
    </script>
</body>
</html>